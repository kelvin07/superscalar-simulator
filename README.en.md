# SuperscalarSimulator

## Introduction

1. Superscalar processor emulator, support dual isue;
2. One integer computing unit and one floating-point computing unit, as well as an independent address computing unit;
3. Two CDBs;
4. Support forward execution at the same time, and submit up to two instructions per cycle;

## Test

Instructions: A cycle is executed three times, and it is assumed that each look is correctly predicted;

```bash
LD.D	F0,0(R1)
ADD.D	F4,F0,F2
SD.D	F4,0(R1)
DADDIU	R1,R1,#-8
BNE		R1,R2,L00P
```

* One int and one FP command can be emitted per cycle
* Branch instructions can only be fired individually, and each prediction is correct, and there is no delay slot
* The Int ALU execution cycle is 1 cycle
* The Load command calculates the address and accesses the memory for one cycle
* FP ALU is performed in 3 cycles

## Output

1. The timing of instruction execution in the three-cycle iteration process of the dual-transmitter processor;
2. Calculate resource utilization and draw tables;