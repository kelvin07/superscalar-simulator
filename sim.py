#!/usr/bin/python
#**coding:utf-8**

# Superscalar Simulator
# Author: yangjingkui
# ID: 23026243
# Create Time: 2023/12/10

debug = 1

# instructions & iterations
inst = ["LD.D F0,0(R1)",
	"ADD.D F4,F0,F2",
	"SD.D F4,0(R1)",
	"DADDIU R1,R1,#-8",
	"BNE R1,R2,L00P",
	]
iter = 3

# maintain global variables
cycle = 1
pc = 0
target_reg = [] # Records the destination register of the variable to be written back, Free when the variable is written on CDB. solve RAW, i.e.data waiting
inst_status = [] # Records the status of instrctions, when commit, update table pipeline
resource_util = [] # Records the utilization of function units by cycle
int_util = 0 # number of times int unit is used
fp_util = 0
addr_util = 0
dcache_util = 0
cdb0_util = 0
cdb1_util = 0

# init instruction status
for i in range(iter):
	for item in inst:
		op, rest = item.split()
		rd, rs2, imm = 0, 0, 0 # default value
		issue = 1 # cycles left, indicate the current status
		execute = 1
		mem_acc = 1
		write = 1
		if op == "ADD.D":
			rd, rs1, rs2 = rest.split(",")
			alu = "fp"
			execute = 3
			mem_acc = 0
		elif op == "DADDIU":
			rd, rs1, rs2 = rest.split(",")
			alu = "int"
			mem_acc = 0
		elif op == "LD.D": 
			rd, addr = rest.split(",")
			imm, rs1 = addr.split("(")
			rs1 = rs1[:-1]
			alu = "addr"
		elif op == "SD.D":
			rs2, addr = rest.split(",")
			imm, rs1 = addr.split("(")
			rs1 = rs1[:-1]
			alu = "addr"
			write = 0
		elif op == "BNE":
			rs1, rs2, _ = rest.split(",")
			alu = "int"
			mem_acc = 0
			write = 0
		else:
			raise Exception("Invalid instruction![sim.py]")
		temp = {"iter":i+1, "inst":item, "op":op, "rd":rd, "rs1":rs1, "rs2":rs2, "imm":imm, "alu":alu, "issue":issue, "execute":execute, "mem_acc":mem_acc, "write":write, "issue_cycle":"", "execute_cycle":"", "mem_acc_cycle":"", "write_cycle":"", "comment":""}
		inst_status.append(temp)

if debug:
	for i in inst_status:
		print(i)

# update by cycle
max_length = iter * len(inst) 
commit = 0

while commit<max_length:
	# init the valid vector, when an instruction is executed in one stage, it becomes invalid, preventing multi-execution in one cycle
	valid = [1,] * max_length
	int_alu = 1
	fp_alu = 1
	addr_alu = 1
	CDB_valid = 2
	resource = {"int":"", "fp":"", "addr":"", "dcache":"", "CDB0":"", "CDB1":""}
	# issue
	if pc == max_length-1:
		inst_status[pc]["issue_cycle"] = cycle
		inst_status[pc]["issue"] -= 1
		valid[pc] = 0
		if inst_status[pc]["rd"] != 0:
			temp = (pc, inst_status[pc]["rd"])
			target_reg.append(temp)
		pc += 1
	elif pc < max_length-1:
		alu1 = inst_status[pc]["alu"]
		alu2 = inst_status[pc+1]["alu"]
		op1 = inst_status[pc]["op"]
		op2 = inst_status[pc+1]["op"]

		if alu1==alu2 or op1=="BNE" or op2=="BNE": # branch must operate separately, only issue one instrction
			# update inst_status
			inst_status[pc]["issue_cycle"] = cycle
			inst_status[pc]["issue"] -= 1
			valid[pc] = 0
			if inst_status[pc]["rd"] != 0:
				temp = (pc, inst_status[pc]["rd"])
				target_reg.append(temp)
			pc += 1
		else: # issue two instrctions
			# update inst_status
			inst_status[pc]["issue_cycle"] = cycle
			inst_status[pc+1]["issue_cycle"] = cycle
			inst_status[pc]["issue"] -= 1
			inst_status[pc+1]["issue"] -= 1
			valid[pc] = 0
			valid[pc+1] = 0
			if inst_status[pc]["rd"] != 0:
				temp = (pc, inst_status[pc]["rd"])
				target_reg.append(temp)
			if inst_status[pc+1]["rd"] != 0:
				temp = (pc+1, inst_status[pc+1]["rd"])
				target_reg.append(temp)
			pc += 2

	# execute
	for idx in range(commit, pc):
		# whether can update
		if inst_status[idx]["issue"]==0 and inst_status[idx]["execute"]>0 and valid[idx]!=0:
			# check RAW
			rs1 = inst_status[idx]["rs1"]
			rs2 = inst_status[idx]["rs2"]
			alu = inst_status[idx]["alu"]
			RAW = 0
			for item in target_reg:
				pc_e, rd_e = item
				if (rs1==rd_e or rs2==rd_e) and idx>pc_e:
					RAW = 1
					op_e = inst_status[pc_e]["op"]
					inst_status[idx]["comment"]="wait for "+op_e
					break
			
			if not RAW:
				if alu == "fp":
					if fp_alu:
						fp_alu -= 1
						res = str(inst_status[idx]["iter"]) + "/" + inst_status[idx]["op"]
						resource["fp"] = res
						fp_util += 1
						if inst_status[idx]["execute"]==3:
							inst_status[idx]["execute_cycle"] = cycle
					else:
						inst_status[idx]["comment"]="wait for FP ALU"
				elif alu == "int":
					if int_alu:
						int_alu -= 1
						res = str(inst_status[idx]["iter"]) + "/" + inst_status[idx]["op"]
						resource["int"] = res
						int_util += 1
						inst_status[idx]["execute_cycle"] = cycle
					else:
						inst_status[idx]["comment"]="wait for INT ALU"
				elif alu == "addr":
					addr_alu -= 1
					res = str(inst_status[idx]["iter"]) + "/" + inst_status[idx]["op"]
					resource["addr"] = res
					addr_util += 1
					inst_status[idx]["execute_cycle"] = cycle
				inst_status[idx]["execute"] -= 1
				valid[idx] = 0


	# memory access
	for idx in range(commit, pc):
		# whether can update
		if inst_status[idx]["issue"]==0 and inst_status[idx]["execute"]==0 and inst_status[idx]["mem_acc"]>0 and valid[idx]!=0:
			inst_status[idx]["mem_acc"] -= 1
			res = str(inst_status[idx]["iter"]) + "/" + inst_status[idx]["op"]
			resource["dcache"] = res
			dcache_util += 1
			inst_status[idx]["mem_acc_cycle"] = cycle
			valid[idx] = 0

	# write CDB
	for idx in range(commit, pc):
		# whether can update
		if inst_status[idx]["issue"]==0 and inst_status[idx]["execute"]==0 and inst_status[idx]["mem_acc"]==0 and inst_status[idx]["write"]>0 and valid[idx]!=0:
			if CDB_valid==2:
				CDB_valid -= 1
				res = str(inst_status[idx]["iter"]) + "/" + inst_status[idx]["op"]
				resource["CDB0"] = res
				cdb0_util += 1
				inst_status[idx]["write"] -= 1
				inst_status[idx]["write_cycle"] = cycle
				rd = inst_status[idx]["rd"]
				temp = (idx, rd)
				if debug: print(cycle, rd)
				target_reg.remove(temp)
				valid[idx] = 0
			elif CDB_valid == 1:
				CDB_valid -= 1
				res = str(inst_status[idx]["iter"]) + "/" + inst_status[idx]["op"]
				resource["CDB1"] = res
				cdb1_util += 1
				inst_status[idx]["write"] -= 1
				inst_status[idx]["write_cycle"] = cycle
				rd = inst_status[idx]["rd"]
				temp = (idx, rd)
				if debug: print(cycle, rd)
				target_reg.remove(temp)
				valid[idx] = 0
			else:
				inst_status[idx]["comment"] = "wait for CDBs"

	# commit (all stages is 0)
	new_commit = commit
	for idx in range(commit, pc): # commit in order
		if inst_status[idx]["issue"]==0 and inst_status[idx]["execute"]==0 and inst_status[idx]["mem_acc"]==0 and inst_status[idx]["write"] == 0:
			new_commit += 1
		else:
			break
	commit = new_commit

	resource_util.append(resource)
	cycle += 1

	if debug:
		print("*"*20)
		print(cycle)
		print(valid)
		print("target_reg", target_reg)
		print("commit=", commit, "pc=", pc)
		for i in inst_status:
			print(i)

cycle -= 1

# init tables and print results
from prettytable import PrettyTable

pipeline_tab = PrettyTable(["Iter.#", "Instructions", "Issue", "Execution", "Mem. Access", "Write CDB", "Comment"])
resource_tab = PrettyTable(["Clock", "Int ALU", "FP ALU", "Addr. Adder", "Data Cache", "CDB0", "CDB1"])

for idx in range(max_length):
	new_row = []
	new_row.append(inst_status[idx]["iter"])
	new_row.append(inst_status[idx]["inst"])
	new_row.append(inst_status[idx]["issue_cycle"])
	new_row.append(inst_status[idx]["execute_cycle"])
	new_row.append(inst_status[idx]["mem_acc_cycle"])
	new_row.append(inst_status[idx]["write_cycle"])
	new_row.append(inst_status[idx]["comment"])
	pipeline_tab.add_row(new_row)

for idx in range(cycle):
	new_row = [idx+1, ]
	new_row.append(resource_util[idx]["int"])
	new_row.append(resource_util[idx]["fp"])
	new_row.append(resource_util[idx]["addr"])
	new_row.append(resource_util[idx]["dcache"])
	new_row.append(resource_util[idx]["CDB0"])
	new_row.append(resource_util[idx]["CDB1"])
	resource_tab.add_row(new_row)

new_row = ["Total", int_util, fp_util, addr_util, dcache_util, cdb0_util, cdb1_util]
resource_tab.add_row(new_row)

new_row = ["Util%", int_util/cycle, fp_util/cycle, addr_util/cycle, dcache_util/cycle, cdb0_util/cycle, cdb1_util/cycle]
resource_tab.add_row(new_row)

print("A Dual-issue Version of Tomasulo Pipeline")
print(pipeline_tab)
print("Resource Usage Table")
print(resource_tab)
print("ExecutionRate=", max_length/cycle)